package marin.calin.lab4.ex4;

import marin.calin.lab4.ex2.Author;

public class Main {
    public static void main(String[] args) {

        var book = new Book("Made in Romania", new Author[] {
                new Author("Peter Earring", "petrica@cercel.com", 'm'),
                new Author("John Earring", "ionut@cercel.com", 'm')
        }, 73.02);
        System.out.println(book);
    }
}
