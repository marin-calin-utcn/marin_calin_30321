package marin.calin.lab4.ex3;

import marin.calin.lab4.ex2.Author;

public class Main {
    public static void main(String[] args) {
        var author = new Author("John Earring", "ionut@cercel.com", 'm');
        var book = new Book("Made in Romania", author, 73.02);
        System.out.println(book);
    }
}
