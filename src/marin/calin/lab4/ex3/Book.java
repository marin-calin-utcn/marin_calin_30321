package marin.calin.lab4.ex3;

import marin.calin.lab4.ex2.Author;

public class Book {
    private String name;
    private Author author;
    private double price;
    private int quantityInStock;

    public Book(String name, Author author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.quantityInStock = 0;
    }

    public Book(String name, Author author, double price, int quantityInStock) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.quantityInStock = quantityInStock;
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    @Override
    public String toString() {
        return "'" + getName() + "' by " + author;
    }
}
