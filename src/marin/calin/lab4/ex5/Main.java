package marin.calin.lab4.ex5;

public class Main {
    public static void main(String[] args) {
        var cylinder = new Cylinder(10, 5);
        System.out.println("Area: " + cylinder.getArea());
        System.out.println("Volume: " + cylinder.getVolume());
    }
}
