package marin.calin.lab4.ex1;

public class Circle {
    private double radius;
    private String colour;

    public Circle() {
        this.radius = 1.0;
        this.colour = "red";
    }

    public Circle(double radius) {
        this.radius = radius;
        this.colour = "red";
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return Math.PI * getRadius() * getRadius();
    }
}
