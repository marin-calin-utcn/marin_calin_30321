package marin.calin.lab4.ex1;

public class Main {
    public static void main(String[] args) {
        var circle = new Circle(6);
        System.out.println("The circle radius is " + circle.getRadius() + " and its area is " + circle.getArea());
    }
}
