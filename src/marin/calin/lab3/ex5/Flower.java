package marin.calin.lab3.ex5;

import java.util.ArrayList;
import java.util.List;

public class Flower{
    public static List<Flower> flowers = new ArrayList<>();
    public static int count = 0;

    public static int getFlowerCount() {
        return count;
    }

    int petal;

    Flower(){
        System.out.println("Flower has been created!");
        flowers.add(this);
        ++count;

    }

    public static void main (String[] args) {
        for(int i = 0; i < 5; i++){
            var f = new Flower();
        }
        System.out.println("Number of created flowers " + getFlowerCount());
    }
}