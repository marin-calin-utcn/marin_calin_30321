package marin.calin.lab3.ex2;

public class CircleTest {
    public static void main(String[] args) {
        var circle = new Circle(3.0, "orange");
        System.out.println("Colour: " + circle.getColour());
        System.out.println("Radius: " + circle.getRadius());
        System.out.println("Area: " + circle.getArea());
    }
}
