package marin.calin.lab3.ex2;

public class Circle {
    private double _radius = 1.0;
    private String _colour = "red";

    public Circle() {
    }

    public Circle(double radius, String colour) {
        this._radius = radius;
        this._colour = colour;
    }

    public double getRadius() {
        return _radius;
    }

    public String getColour() {
        return _colour;
    }

    public double getArea() {
        return Math.PI * this._radius * this._radius;
    }
}
