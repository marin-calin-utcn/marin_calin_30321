package marin.calin.lab3.ex3;

public class Author {
    private String _name;
    private String _email;
    private char _gender;

    public Author(String name, String email, char gender) {
        _name = name;
        _email = email;
        _gender = gender;
    }

    public String getName() {
        return _name;
    }

    public String getEmail() {
        return _email;
    }

    public void setEmail(String _email) {
        this._email = _email;
    }

    public char getGender() {
        return _gender;
    }

    @Override
    public String toString() {
        return _name + " (" + _gender + ") at " + _email;
    }
}
