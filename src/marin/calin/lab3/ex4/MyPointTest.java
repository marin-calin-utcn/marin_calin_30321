package marin.calin.lab3.ex4;

public class MyPointTest {
    public static void main(String[] args) {
        var firstPoint = new MyPoint();
        var secondPoint = new MyPoint(7.0, 52.2);
        System.out.println("Distance from " + firstPoint + " to " + secondPoint + " is " + firstPoint.calculateDistance(secondPoint));
        System.out.println("Distance from " + firstPoint + " to (66.6, 77.7) is " + firstPoint.calculateDistance(66.6, 77.7));
    }
}
