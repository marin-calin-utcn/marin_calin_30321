package marin.calin.lab1;

// Test comment
public class HelloWorld {
    /**
     * Application entry point.
     *
     * @param args Arguments received at startup.
     */
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
