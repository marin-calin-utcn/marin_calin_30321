package marin.calin.lab2.ex2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    /**
     * Application entry point.
     *
     * @param args Application arguments.
     */
    public static void main(String[] args) throws IOException {
        var reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("What number do you want to format?:");
        var input = Integer.parseInt(reader.readLine());
        System.out.println(_getFormattedDigit(input));
    }

    /**
     * Get a string formatted version of an integer.
     *
     * @param input The integer that is to be formatted.
     * @return The formatted output.
     */
    private static String _getFormattedDigit(int input) {
//        Version that uses IF statements
//        if(input == 1) return ("ONE");
//        else if(input == 2) return ("TWO");
//        else if(input == 3) return ("THREE");
//        else if(input == 4) return ("FOUR");
//        else if(input == 5) return ("FIVE");
//        else if(input == 6) return ("SIX");
//        else if(input == 7) return ("SEVEN");
//        else if(input == 8) return ("EIGHT");
//        else if(input == 9) return ("NINE");
//        else return ("OTHER");

        return switch (input) {
            case 0 -> "ZERO";
            case 1 -> "ONE";
            case 2 -> "TWO";
            case 3 -> "THREE";
            case 4 -> "FOUR";
            case 5 -> "FIVE";
            case 6 -> "SIX";
            case 7 -> "SEVEN";
            case 8 -> "EIGHT";
            case 9 -> "NINE";
            default -> "OTHER";
        };
    }
}
