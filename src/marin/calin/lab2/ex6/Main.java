package marin.calin.lab2.ex6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    /**
     * Application entry point.
     *
     * @param args Application arguments.
     */
    public static void main(String[] args) throws IOException {
        var reader = new BufferedReader(new InputStreamReader(System.in));

        // Read the input array
        System.out.println("Type the number whose factorial you want to compute:");
        var n = Integer.parseInt(reader.readLine());

        // Print the result
        System.out.println("n! computed directly is " + _factorial(n));
        System.out.println("n! computed recursively is " + _factorialRecursive(n));
    }

    /**
     * Compute the factorial of a number.
     *
     * @param n The input value.
     * @return The computed factorial.
     */
    private static int _factorial(int n) {
        int factorial = 1;
        for (int i = 2; i <= n; i++) {
            factorial *= i;
        }
        return factorial;
    }

    /**
     * Compute the factorial of a number in a recursive manner.
     *
     * @param n The input value.
     * @return The computed factorial.
     */
    private static int _factorialRecursive(int n) {
        if (n == 1)
            return 1;

        return n * _factorialRecursive(n - 1);
    }
}
