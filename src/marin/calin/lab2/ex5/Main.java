package marin.calin.lab2.ex5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;



public class Main {
    private static final double MAX_VALUE = 666;
    
    /**
     * Application entry point.
     *
     * @param args Application arguments.
     */
    public static void main(String[] args) {
        var size = (int) (Math.random() * 15);
        var v = new int[size];

        // Generate the vector
        for (int i = 0; i < size; i++) {
            v[i] = (int) (Math.random() * MAX_VALUE);
        }
        System.out.println("The generated vector is:");
        _printVector(v);

        // Sort the array
        _bubbleSort(v);
        System.out.println("The sorted vector is:");
        _printVector(v);
    }

    /**
     * Print an array.
     * @param array The array.
     */
    private static void _printVector(int[] array) {
        for (int value : array) {
            System.out.print(value + " ");
        }
        System.out.println();
    }

    /**
     * Run the bubble sort algorithm on an array.
     *
     * @param array The array to be sorted.
     * @see <a href="https://www.geeksforgeeks.org/bubble-sort/">Geeks for Geeks</a>
     */
    private static void _bubbleSort(int[] array)
    {
        int n = array.length;
        for (int i = 0; i < n-1; i++) {
            for (int j = 0; j < n-i-1; j++) {
                if (array[j] > array[j+1])
                {
                    // swap array[j+1] and array[j]
                    int temp = array[j];
                    array[j] = array[j+1];
                    array[j+1] = temp;
                }
            }
        }
    }
}
