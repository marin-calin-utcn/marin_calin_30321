package marin.calin.lab2.ex1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    /**
     * Application entry point.
     *
     * @param args Application arguments.
     */
    public static void main(String[] args) throws IOException {
        var reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Type the first number: ");
        var first = Integer.parseInt(reader.readLine());
        System.out.println("Type the second number: ");
        var second = Integer.parseInt(reader.readLine());

        System.out.println("The greatest number between " + first + " and " + second + " is " + Math.max(first, second) + ".");
    }
}
