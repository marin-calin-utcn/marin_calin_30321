package marin.calin.lab2.ex4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    /**
     * Application entry point.
     *
     * @param args Application arguments.
     */
    public static void main(String[] args) throws IOException {
        var reader = new BufferedReader(new InputStreamReader(System.in));

        // Read the input array
        System.out.println("What is the length of the input array?");
        var n = Integer.parseInt(reader.readLine());
        var v = new int[n];
        for (int i = 0; i < n; i++) {
            System.out.print("v[" + i + "] = ");
            v[i] = Integer.parseInt(reader.readLine());
        }

        // Figure out the maximum value
        var max = v[0];
        for (int i = 1; i < n; i++) {
            max = Math.max(max, v[i]);
        }

        // Print the result
        System.out.println("The maximum value from the input is " + max);
    }
}
