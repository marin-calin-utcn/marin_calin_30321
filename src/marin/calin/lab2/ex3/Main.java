package marin.calin.lab2.ex3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    /**
     * Application entry point.
     *
     * @param args Application arguments.
     */
    public static void main(String[] args) throws IOException {
        var reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("What are the lower bounds of the interval?");
        var a = Integer.parseInt(reader.readLine());
        System.out.println("What are the upper bounds of the interval?");
        var b = Integer.parseInt(reader.readLine());

        for (int i = a; i < b; i++) {
            if (_isPrime(i)) {
                System.out.println(i);
            }
        }
    }

    /**
     * Check if a given number is prime.
     *
     * @param n The number that is to be checked.
     * @return Whether the number is prime or not.
     */
    private static boolean _isPrime(int n) {
        if (n == 1) return false;
        if (n % 2 == 0 && n > 2) return false;

        for (int i = 3; i <= n / 2; i += 2) {
            // condition for non-prime number
            if (n % i == 0) {
                return false;
            }
        }

        return true;
    }
}
