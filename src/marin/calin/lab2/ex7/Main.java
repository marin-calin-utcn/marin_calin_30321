package marin.calin.lab2.ex7;

import java.io.IOException;

public class Main {
    /**
     * Application entry point.
     *
     * @param args Application arguments.
     */
    public static void main(String[] args) {
        var app = new Game();
        try {
            var won = app.run();
            if (won) {
                System.out.println("You've won!");
            } else {
                System.out.println("You've lost!");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
