package marin.calin.lab2.ex7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Game {
    public final static int MAX_NUMBER = 10;
    public final static int MAX_TRIES = 3;

    public int triesLeft = MAX_TRIES;

    private final int _number;

    /**
     * App constructor method.
     */
    public Game() {
        _number = (int) (Math.random() * MAX_NUMBER);
    }

    /**
     * Run the application.
     *
     * @return Whether the user won or lost the game.
     */
    public boolean run() throws IOException {
        var reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("A random number between 0 and " + MAX_NUMBER + " has been generated.");
        System.out.println("You have " + MAX_TRIES + " chances to guess it right.");

        while (triesLeft > 0) {
            System.out.println("What is your guess?");
            var n = Integer.parseInt(reader.readLine());
            if (n == _number) {
                return true;
            } else if (n > _number) {
                System.out.println("Wrong answer, your number it too high!");
            } else {
                System.out.println("Wrong answer, your number is too low!");
            }
            --triesLeft;
        }

        return false;
    }
}
