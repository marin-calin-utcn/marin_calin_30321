package marin.calin.lab8.ex3;

class Train{
    String destination;
    String name;

    public Train(String destination, String nume) {
        super();
        this.destination = destination;
        this.name = nume;
    }

    String getDestination(){
        return destination;
    }

}