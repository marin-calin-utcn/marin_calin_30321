package marin.calin.lab8.ex4;

import marin.calin.lab8.ex4.events.Event;

public class HomeAutomation {

    public static void main(String[] args){
        var controlUnit = ControlUnit.make();

        // test using an anonymous inner class
        var home = new Home() {
            protected void setValueInEnvironment(Event event) {
                controlUnit.handleEvent(event);
                System.out.println("New event in environment " + event);
            }
            protected void controlStep(){
                System.out.println("Control step executed");
            }
        };
        home.simulate();
    }
}
