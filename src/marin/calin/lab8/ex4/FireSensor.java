package marin.calin.lab8.ex4;

public class FireSensor {
    private boolean active;

    public FireSensor() {
        this(false);
    }

    public FireSensor(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
