package marin.calin.lab8.ex4.events;

public class NoEvent extends Event {
    public NoEvent() {
        super(EventType.None);
    }

    @Override
    public String toString() {
        return "NoEvent { }";
    }
}
