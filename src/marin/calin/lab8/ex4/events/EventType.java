package marin.calin.lab8.ex4.events;

public enum EventType {
    Temperature,
    Fire,
    None
}
