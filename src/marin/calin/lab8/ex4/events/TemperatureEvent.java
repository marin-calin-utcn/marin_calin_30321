package marin.calin.lab8.ex4.events;

public class TemperatureEvent extends Event {

    private final int value;

    public TemperatureEvent(int value) {
        super(EventType.Temperature);
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "TemperatureEvent { " + "value = " + value + " }";
    }
}
