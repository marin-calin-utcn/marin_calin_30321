package marin.calin.lab8.ex4.events;

public class FireEvent extends Event {

    private final boolean smoke;

    public FireEvent(boolean smoke) {
        super(EventType.Fire);
        this.smoke = smoke;
    }

    boolean isSmoke() {
        return smoke;
    }

    @Override
    public String toString() {
        return "FireEvent {" + " smoke = " + smoke + " }";
    }
}
