package marin.calin.lab8.ex4.events;

public abstract class Event {
    EventType type;

    Event(EventType type) {
        this.type = type;
    }

    public EventType getType() {
        return type;
    }
}
