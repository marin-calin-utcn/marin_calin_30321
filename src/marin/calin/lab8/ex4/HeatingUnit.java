package marin.calin.lab8.ex4;

public class HeatingUnit {
    private boolean active = false;

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean getActive() {
        return active;
    }
}
