package marin.calin.lab8.ex4.logging;

import marin.calin.lab8.ex4.events.Event;

public interface ILogService {
    void logEvent(Event event);
}
