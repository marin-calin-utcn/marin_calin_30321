package marin.calin.lab8.ex4;

import marin.calin.lab8.ex4.events.Event;
import marin.calin.lab8.ex4.events.FireEvent;
import marin.calin.lab8.ex4.events.NoEvent;
import marin.calin.lab8.ex4.events.TemperatureEvent;
import marin.calin.lab8.ex4.logging.FileLogService;
import marin.calin.lab8.ex4.logging.ILogService;

import java.util.Random;

public abstract class Home {
    private final Random r = new Random();
    private final int SIMULATION_STEPS = 20;
    private final ILogService logService = new FileLogService();

    protected abstract void setValueInEnvironment(Event event);
    protected abstract void controlStep();

    private Event getHomeEvent() {
        // randomly generate a new event;
        int k = r.nextInt(100);
        if(k < 30) {
            return new NoEvent();
        } else if(k < 60) {
            return new FireEvent(r.nextBoolean());
        } else {
            return new TemperatureEvent(r.nextInt(50));
        }
    }

    public void simulate() {
        int k = 0;
        while(k < SIMULATION_STEPS){
            var event = this.getHomeEvent();
            setValueInEnvironment(event);
            controlStep();
            logService.logEvent(event);

            try {
                Thread.sleep(300);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }

            k++;
        }
    }
}
