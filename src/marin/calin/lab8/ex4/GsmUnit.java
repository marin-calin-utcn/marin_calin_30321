package marin.calin.lab8.ex4;

import marin.calin.lab8.ex4.events.Event;

public class GsmUnit {
    public void alertOwner(Event event) {
        System.out.println("Calling the owner about an event: " + event);
    }
}
