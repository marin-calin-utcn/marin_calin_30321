package Lab11.Exercise1;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;
import java.util.Observable;
import java.util.Observer;

public class View implements Observer {
    JFrame frame;
    JLabel label;
    JTextField textField;
    JButton button;

    public View() {
        frame = new JFrame("Exercise 1");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(350, 100);
        frame.setLocationRelativeTo(null);
        frame.setLayout(null);

        label = new JLabel("Temperature: ");
        label.setBounds(10, 10, 100, 20);
        frame.add(label);

        textField = new JTextField();
        textField.setBounds(100, 10, 100, 20);
        textField.setFont(new Font("Arial", Font.BOLD, 15));
        textField.setHorizontalAlignment(JTextField.CENTER);
        frame.add(textField);

        button = new JButton("Enable/Disable");
        button.setBounds(210, 10, 120, 20);
        button.setFont(new Font("Arial", Font.PLAIN, 12));
        frame.add(button);

        frame.setVisible(true);
    }

    @Override
    public void update(Observable o, Object arg) {
        DecimalFormat decimalFormat = new DecimalFormat();
        decimalFormat.setMaximumFractionDigits(2);
        float temperature = Float.parseFloat(decimalFormat.format(((TemperatureSensor) o).getTemp()));
        String str = temperature + " C";

        textField.setText(str);
    }

    public void addEnableDisableListener(Controller.EnableDisableListener listener) {
        button.addActionListener(listener);
    }
}
