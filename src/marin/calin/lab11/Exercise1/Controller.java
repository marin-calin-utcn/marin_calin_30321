package Lab11.Exercise1;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
    View view;
    TemperatureSensor temperatureSensor;

    public Controller(TemperatureSensor temperatureSensor, View view) {
        temperatureSensor.addObserver(view);
        this.temperatureSensor = temperatureSensor;
        this.view = view;

        view.addEnableDisableListener(new EnableDisableListener());
    }

    class EnableDisableListener implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            temperatureSensor.setPause(!temperatureSensor.isPaused());
        }

    }

}
