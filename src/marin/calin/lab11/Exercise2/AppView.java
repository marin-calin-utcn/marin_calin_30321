package Lab11.Exercise2;

import javax.swing.*;

public class AppView {

    JFrame jFrame;
    JButton addButton, viewButton, deleteButton, quantityButton;
    JTextArea textArea;
    JTextField nameTF, priceTF, quantityTF;
    JLabel nameLabel, priceLabel, quantityLabel;

    AppModel model = new AppModel();

    public AppView() {
        jFrame = new JFrame("AppView");
        jFrame.setLayout(null);
        textArea = new JTextArea("Products\n");
        textArea.setBounds(30, 0, 300, 300);

        nameTF = new JTextField();
        nameTF.setBounds(150, 330, 100, 30);

        priceTF = new JTextField();
        priceTF.setBounds(150, 360, 100, 30);

        quantityTF = new JTextField();
        quantityTF.setBounds(150, 390, 100, 30);

        jFrame.setSize(400, 625);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        addButton = new JButton("Add");
        addButton.setBounds(30, 430, 100, 50);

        viewButton = new JButton("View");
        viewButton.setBounds(150, 430, 100, 50);

        deleteButton = new JButton("Delete");
        deleteButton.setBounds(30, 500, 100, 50);

        quantityButton = new JButton("Quantity");
        quantityButton.setBounds(150, 500, 100, 50);

        nameLabel = new JLabel("Name");
        nameLabel.setBounds(30, 330, 100, 30);

        priceLabel = new JLabel("Price");
        priceLabel.setBounds(30, 360, 100, 30);

        quantityLabel = new JLabel("Quantity");
        quantityLabel.setBounds(30, 390, 100, 30);

        jFrame.add(textArea);
        jFrame.add(nameLabel);
        jFrame.add(nameTF);

        jFrame.add(priceLabel);
        jFrame.add(priceTF);

        jFrame.add(quantityLabel);
        jFrame.add(quantityTF);

        jFrame.add(addButton);
        jFrame.add(viewButton);
        jFrame.add(deleteButton);
        jFrame.add(quantityButton);

        jFrame.setVisible(true);
    }

    public void setTextArea(String text) {
        textArea.setText(text);
    }

    public String getName() {
        return nameTF.getText();
    }

    public int getQuantity() {
        return Integer.parseInt(quantityTF.getText());
    }

    public double getPrice() {
        return Double.parseDouble(priceTF.getText());
    }

}
