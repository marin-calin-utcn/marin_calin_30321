package marin.calin.lab6.ex1;

import java.util.Objects;

public class BankAccount implements Comparable<BankAccount> {
    private final String owner;
    private double balance;

    public BankAccount(String owner) {
        this.owner = owner;
        this.balance = 0;
    }

    public BankAccount(String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount) throws IllegalArgumentException {
        if (amount < 0) {
            throw new IllegalArgumentException("The withdrawn amount cannot be lower than zero!");
        }

        if (balance >= amount) {
            throw new IllegalArgumentException("There is not enough money in the bank account!");
        }

        this.balance -= amount;
    }

    public void deposit(double amount) throws IllegalArgumentException {
        if (amount < 0) {
            throw new IllegalArgumentException("The deposited amount cannot be lower than zero!");
        }

        this.balance += amount;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getOwner() {
        return owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        var that = (BankAccount) o;
        return Double.compare(that.balance, balance) == 0 && owner.equals(that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    @Override
    public String toString() {
        return "Account belonging to " + owner + " ($" + balance + ")";
    }

    @Override
    public int compareTo(BankAccount o) {
        return (int)(this.getBalance() - o.getBalance());
    }
}
