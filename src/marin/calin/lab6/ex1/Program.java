package marin.calin.lab6.ex1;

public class Program {
    public static void main(String[] args) {
        var myAccount = new BankAccount("John Earring", 54);
        var theirAccount = new BankAccount("Peter Earring", 123);
        var anotherAccount = new BankAccount("John Earring", 54);

        System.out.println(myAccount + " is the same as " + theirAccount + ": " + myAccount.equals(theirAccount));
        System.out.println(myAccount + " is the same as " + anotherAccount + ": " + myAccount.equals(anotherAccount));
    }
}
