package marin.calin.lab6.ex2;

import marin.calin.lab6.ex1.BankAccount;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Bank {
    protected ArrayList<BankAccount> accounts = new ArrayList<>();

    public void addAccount(BankAccount ...accounts) {
        this.accounts.addAll(List.of(accounts));
    }

    public void printAccounts() {
        this.accounts.stream()
                .sorted()
                .forEach(System.out::println);
    }

    public void printAccounts(double minBalance, double maxBalance) {
        accounts.stream()
                .filter(account -> account.getBalance() >= minBalance && account.getBalance() <= maxBalance)
                .forEach(System.out::println);
    }

    public Optional<BankAccount> getAccount(String owner) {
        return this.accounts.stream()
                .filter(account -> account.getOwner().equalsIgnoreCase(owner))
                .findFirst();
    }
}
