package marin.calin.lab6.ex3;

import marin.calin.lab6.ex1.BankAccount;

public class Program {
    public static void main(String[] args) {
        var bank = new Bank();
        bank.addAccount(
            new BankAccount("John Earring", 64),
            new BankAccount("Peter Earring", 123),
            new BankAccount("Vali Thunderstorm", 0),
            new BankAccount("Sorin Littleone", 88),
            new BankAccount("Adrian Wonder", 63)
        );

        System.out.println("== Bank Accounts ==");
        bank.printAccounts();

        System.out.println("== Bank Accounts ($100 - $200) ==");
        bank.printAccounts(100, 200);

        System.out.println("== John Earring's Bank Account ==");
        var account = bank.getAccount("john earring");
        if (account.isPresent()) {
            System.out.println(account.orElseThrow());
        }
    }
}
