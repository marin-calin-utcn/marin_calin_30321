package marin.calin.lab6.ex3;

import marin.calin.lab6.ex1.BankAccount;

import java.util.List;
import java.util.Optional;
import java.util.TreeSet;

public class Bank  {
    protected TreeSet<BankAccount> accounts = new TreeSet<>();

    public void addAccount(BankAccount ...accounts) {
        this.accounts.addAll(List.of(accounts));
    }

    public void printAccounts() {
        for (var account : this.accounts) {
            System.out.println(account);
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (var account : accounts) {
            if (account.getBalance() >= minBalance && account.getBalance() <= maxBalance) {
                System.out.println(account);
            }
        }
    }

    public Optional<BankAccount> getAccount(String owner) {
        return this.accounts.stream().filter(account -> account.getOwner().equalsIgnoreCase(owner)).findFirst();
    }
}
