package marin.calin.lab6.ex4;

public class Definition {
    private final String description;

    public Definition(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
