package marin.calin.lab6.ex4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ConsoleMenu {

    enum Action {
        PrintDictionary,
        AddToDictionary,
        GetDefinition,
        GetAllDefinitions,
        RemoveFromDictionary,
        Quit
    }

    public static void printMenu() {
        var i = 0;
        for (var action : Action.values()) {
            System.out.println("[" + i + "] " + action);
            ++ i;
        }
    }

    public static void main(String[] args) throws IOException {
        var reader = new BufferedReader(new InputStreamReader(System.in));
        var dictionary = new Dictionary();
        dictionary.addWord(new Word("test"), new Definition("dadaada"));

        Action action = null;
        do {
            printMenu();

            try {
                action = Action.values()[Integer.parseInt(reader.readLine())];
            } catch (Exception exception) {
                continue;
            }

            switch (action) {
                case PrintDictionary -> {
                    for (var word : dictionary.getAllWords()) {
                        System.out.println(word + " = " + dictionary.getDefinition(word));
                    }
                }
                case AddToDictionary -> {
                    System.out.println("What word do you want to add?");
                    var word = reader.readLine();
                    System.out.println("What is this word's definition?");
                    var definition = reader.readLine();
                    dictionary.addWord(new Word(word), new Definition(definition));
                    System.out.println("Word added to the dictionary.");
                }
                case GetDefinition -> {
                    System.out.println("What word do you want to find?");
                    var word = reader.readLine();
                    try {
                        var definition = dictionary.getDefinition(new Word(word));
                        System.out.println("The definition for " + word + " is " + definition);
                    } catch (Exception exception) {
                        System.out.println("There is no definition for this word!");
                    }
                }
                case GetAllDefinitions -> {
                    dictionary.getDefinitions().forEach(System.out::println);
                }
                case RemoveFromDictionary -> {
                    System.out.println("What word do you want to remove?");
                    var word = reader.readLine();
                    dictionary.removeWord(new Word(word));
                    System.out.println("Word removed successfully!");
                }
            }
        } while (action != Action.Quit);
    }
}
