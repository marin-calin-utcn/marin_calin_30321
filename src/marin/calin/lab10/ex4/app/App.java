package marin.calin.lab10.ex4.app;

import marin.calin.lab10.ex4.app.enums.MoveDirection;
import marin.calin.lab10.ex4.app.models.Position;
import marin.calin.lab10.ex4.app.models.Robot;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class App implements Runnable {
    static int BOARD_SIZE = 4;

    private List<Robot> robots = new ArrayList<>();
    private Thread thread;

    public App() {
        // Generate robots
        this.robots.add(new Robot("Robot one", this.generateEmptyPosition()));
        this.robots.add(new Robot("Robot two", this.generateEmptyPosition()));
        this.robots.add(new Robot("Robot three", this.generateEmptyPosition()));
        this.robots.add(new Robot("Robot four", this.generateEmptyPosition()));
        this.robots.add(new Robot("Robot five", this.generateEmptyPosition()));
        this.robots.add(new Robot("Robot six", this.generateEmptyPosition()));
        this.robots.add(new Robot("Robot seven", this.generateEmptyPosition()));
        this.robots.add(new Robot("Robot eight", this.generateEmptyPosition()));
        this.robots.add(new Robot("Robot nine", this.generateEmptyPosition()));
        this.robots.add(new Robot("Robot ten", this.generateEmptyPosition()));
    }

    @Override
    public void run() {
        try {
            while ((long) this.robots.size() > 1) {
                // Move the robots
                for (var robot : this.robots) {
                    var move = this._generateValidMove(robot);
                    robot.move(move);
                }

                // Check for collisions
                this._printBoard();
                for (var robot : this.robots) {
                    var collided = this.robots.stream().anyMatch(otherRobot -> otherRobot != robot && Objects.equals(otherRobot.getPosition(), robot.getPosition()));

                    if (collided) {
                        this._destroyRobot(robot);
                    }

                }

                // Print the board

                // Wait for 100ms
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private MoveDirection _generateValidMove(Robot robot) {
        var valid = true;
        MoveDirection move;
        do {
            move = MoveDirection.values()[(int)(Math.random() * MoveDirection.values().length)];
            switch (move) {
                case Up -> valid = robot.getPosition().getY() > 0;
                case Right -> valid = robot.getPosition().getX() < BOARD_SIZE - 1;
                case Down -> valid = robot.getPosition().getY() < BOARD_SIZE - 1;
                case Left -> valid = robot.getPosition().getX() > 0;
            }
        } while (!valid);

        return move;
    }

    private void _destroyRobot(Robot robot) {
        this.robots = this.robots.stream().filter(r -> !r.equals(robot)).toList();
    }

    public void start() {
        if (this.thread == null) {
            this.thread = new Thread(this, "Application");
            this.thread.start();
        }
    }

    private void _printBoard() {
        System.out.println("==== BOARD (" + BOARD_SIZE + "x" + BOARD_SIZE + ") ====");
        System.out.println("Remaining: " + this.robots.size());
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                // Check if this place is occupied
                var occupied = false;
                for (var robot : this.robots) {
                    if (Objects.equals(robot.getPosition(), new Position(i, j))) {
                        occupied = true;
                    }
                }

                if (occupied) {
                    System.out.print("* ");
                } else {
                    System.out.print("- ");
                }
            }
            System.out.print(System.lineSeparator());
        }
        System.out.println("===============");
    }

    private Position generateEmptyPosition() {
        var ref = new Object() {
            Position position;
        };
        do {
            ref.position = new Position((int) Math.floor(Math.random() * BOARD_SIZE), (int) Math.floor(Math.random() * BOARD_SIZE));
        } while (robots.stream().anyMatch(r -> ref.position.equals(r.getPosition())));

        return ref.position;
    }
}
