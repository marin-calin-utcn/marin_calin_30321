package marin.calin.lab10.ex4.app.enums;

public enum MoveDirection {
    Up,
    Right,
    Down,
    Left
}
