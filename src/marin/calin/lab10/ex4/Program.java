package marin.calin.lab10.ex4;

import marin.calin.lab10.ex4.app.App;

public class Program {
    public static void main(String[] args) {
        var app = new App();
        app.start();
    }
}
