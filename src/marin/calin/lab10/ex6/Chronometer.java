package marin.calin.lab10.ex6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Chronometer implements ActionListener {
    JFrame frame;
    JButton button1;
    JButton button2;
    JTextField textField;
    boolean buttonState = false;
    int count = 0;

    public Chronometer() {
        frame = new JFrame("Chronometer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 180);
        frame.setLocationRelativeTo(null);
        frame.setLayout(null);

        button1 = new JButton("Start");
        button1.setBounds(10, 10, 120, 50);
        button1.addActionListener(this);
        frame.add(button1);

        button2 = new JButton("Reset");
        button2.setBounds(150, 10, 120, 50);
        button2.addActionListener(this);
        frame.add(button2);

        textField = new JTextField();
        textField.setBounds(70, 70, 150, 50);
        textField.setFont(new Font("Arial", Font.BOLD, 20));
        textField.setHorizontalAlignment(JTextField.CENTER);
        frame.add(textField);

        frame.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == button1) {
            if (!buttonState) {
                button1.setText("Stop");
                buttonState = true;

                new Thread(new Runnable() {
                    public void run() {
                        while (buttonState) {
                            try {
                                Thread.sleep(1000);
                                textField.setText(String.valueOf(count));
                                count++;
                            } catch (InterruptedException ex) {
                                System.out.println(ex.getMessage());
                            }
                        }
                    }
                }).start();
            } else {
                button1.setText("Start");
                buttonState = false;
            }
        }
        else if (e.getSource() == button2) {
            count = 0;
            buttonState = false;
        }
    }

    public static void main(String[] args) {
        new Chronometer();
    }
}
