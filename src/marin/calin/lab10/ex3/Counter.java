package Lab10.Exercise3;

public class Counter extends Thread {
    private int count = 0;
    private int max;
    Thread t;

    public Counter(int max, Thread t) {
        this.max = max;
        this.t = t;
    }
    public Counter(int count, int max, Thread t) {
        this.count = count;
        this.max = max;
        this.t = t;
    }

    public void run() {
        if(t != null) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        while (count <= max) {
            System.out.println(count);
            count++;
        }
    }
}
