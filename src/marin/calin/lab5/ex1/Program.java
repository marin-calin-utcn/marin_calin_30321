package marin.calin.lab5.ex1;

public class Program {
    public static void main(String[] args) {
        var circle = new Circle(10, "red", false);
        var rectangle = new Rectangle(6, 4, "purple", false);
        var square = new Square(5, "orange", true);

        // Print values
        System.out.println(circle);
        System.out.println(rectangle);
        System.out.println(square);
    }
}
