package marin.calin.lab5.ex1;

public class Rectangle extends Shape {
    protected double width;
    protected double length;

    public Rectangle() {
        super();
    }

    @Override
    public double getArea() {
        return getWidth() * getLength();
    }

    @Override
    public double getPerimeter() {
        return 2 * (getLength() + getWidth());
    }

    @Override
    public String toString() {
        return "Rectangle { " +
                "width = " + width +
                ", length = " + length +
                ", colour = '" + colour + '\'' +
                ", filled = " + filled +
                ", area = " + getArea() +
                ", perimeter = " + getPerimeter() +
                " }";
    }

    public Rectangle(double width, double length) {
        super();
        this.width = width;
        this.length = length;
    }

    public Rectangle(double width, double length, String colour, boolean filled) {
        super(colour, filled);
        this.width = width;
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }
}
