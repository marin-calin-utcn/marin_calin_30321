package marin.calin.lab5.ex4;

public class Program {
    public static void main(String[] args) throws InterruptedException {
        var controller = ControllerSingleton.create();
        controller.control();
    }
}
