package marin.calin.lab5.ex4;

import marin.calin.lab5.ex3.Controller;

public class ControllerSingleton extends Controller {
    private static volatile ControllerSingleton controller = null;

    private ControllerSingleton() {
    }

    public static ControllerSingleton create() {
        synchronized (ControllerSingleton.class) {
            if (controller == null) {
                controller = new ControllerSingleton();
            }
        }
        return controller;
    }
}
