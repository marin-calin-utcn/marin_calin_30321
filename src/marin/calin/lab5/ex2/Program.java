package marin.calin.lab5.ex2;

public class Program {
    public static void main(String[] args) {
        var real = new ProxyImage("image.png", "real");
        var rotated = new ProxyImage("image.jpg", "rotated");

        real.display();
        rotated.display();
    }
}
