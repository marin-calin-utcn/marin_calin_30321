package marin.calin.lab5.ex2;

public class RotatedImage implements Image {
    private final String fileName;

    public RotatedImage(String fileName) {
        this.fileName = fileName;
        this.loadFromDisk(this.fileName);
    }

    @Override
    public void display() {
        System.out.println("Displaying rotated " + fileName);
    }

    private void loadFromDisk(String fileName){
        System.out.println("Loading " + fileName);
    }
}
