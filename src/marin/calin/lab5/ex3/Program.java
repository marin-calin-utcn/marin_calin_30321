package marin.calin.lab5.ex3;

public class Program {
    public static void main(String[] args) throws InterruptedException {
        var controller = new Controller();
        controller.control();
    }
}
