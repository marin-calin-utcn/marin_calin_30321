package marin.calin.lab7.ex1;

public class Program {
    public static void main(String[] args) {
        var maker = new CoffeeMaker();

        for (int i = 0; i < CoffeeMaker.maxCoffees + 3; i++) {
            try {
                var coffee = maker.makeCoffee();
                System.out.println(coffee);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
