package marin.calin.lab7.ex3;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.util.Arrays;

public class Program {
    public static void main(String[] args) throws IOException {
        var reader = new BufferedReader(new InputStreamReader(System.in));
        Action action;
        do {
            writeMenu();
            try {
                action = Action.valueOf(reader.readLine());
            } catch (Exception e) {
                action = null;
            }
        } while (action == null);

        var encrypter = new Encrypter();

        switch (action) {
            case Encrypt -> {
                // Get the file path
                System.out.println("What file do you want to encrypt?:");
                var filePath = reader.readLine();

                // Get the input data
                var data = getInput(filePath);

                // Write the encrypted data
                encrypter.encryptText(data).writeToFile(filePath.replaceAll(".txt", ".enc"));
            }
            case Decrypt -> {
                // Get the file path
                System.out.println("What file do you want to decrypt?:");
                var filePath = reader.readLine();

                // Get the encrypted data
                var data = getInput(filePath);

                // Decrypt the data
                encrypter.decryptText(data).writeToFile(filePath.replaceAll(".enc", ".dec"));
            }
        }
    }

    private static String getInput(String path) throws IOException {
        var file = new File(path);
        return new String(Files.readAllBytes(file.toPath()));
    }

    private static void writeMenu() {
        System.out.println("======== Menu ========");
        Arrays.stream(Action.values()).forEach(System.out::println);
        System.out.println("======================");

    }
}
