package marin.calin.lab7.ex3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Encrypter {
    private String _encrypted;

    public Encrypter() {

    }

    public Encrypter encryptText(String input) {
        this._encrypted = input.chars()
                .map(ch -> ch - 1)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return this;
    }

    public Encrypter decryptText(String input) {
        this._encrypted = input.chars()
                .map(ch -> ch + 1)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return this;
    }

    public void writeToFile(String path) throws IOException {
        var myObj = new File(path);
        if (myObj.createNewFile()) {
            System.out.println("File created: " + myObj.getName());
        } else {
            System.out.println("File updated: " + myObj.getName());
        }
        var writer = new FileWriter(path);
        writer.write(this._encrypted);
        writer.close();
    }
}
