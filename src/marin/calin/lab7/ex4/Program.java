package marin.calin.lab7.ex4;

import marin.calin.lab7.ex4.app.App;

import java.io.IOException;

public class Program {
    public static void main(String[] args) throws IOException {
        var app = new App();
        app.run();
    }
}
