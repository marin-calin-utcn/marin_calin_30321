package marin.calin.lab7.ex4.app.services;

import marin.calin.lab7.ex4.app.enums.Action;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MenuService {
    /**
     * Print the available actions.
     */
    public void showMenu() {
        System.out.println("====== Menu ======");
        for (var action : Action.values()) {
            System.out.println(action);
        }
        System.out.println("==================");
    }

    /**
     * Get the desired action from keyboard input.
     *
     * @return The requested action.
     */
    public Action getAction() {
        var reader = new BufferedReader(new InputStreamReader(System.in));
        Action action;
        do {
            System.out.println("What action do you wish to perform?");
            try {
                var actionText = reader.readLine();
                action = Action.valueOf(actionText);
            } catch (Exception e) {
                e.printStackTrace();
                action = null;
            }
        } while (action == null);

        return action;
    }
}
