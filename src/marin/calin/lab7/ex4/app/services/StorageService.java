package marin.calin.lab7.ex4.app.services;

import marin.calin.lab7.ex4.app.models.Car;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class StorageService {
    private final String directory;

    public StorageService(String directory) {
        this.directory = directory;
        this.ensureDirectoryExists();
    }

    private boolean ensureDirectoryExists() {
        var file = new File(this.directory);
        return file.mkdirs();
    }

    public void store(Car car) throws IOException {
        var file = getCarPath(car).toFile();
        while (!file.createNewFile()) {
            file.delete();
        }
        var writer = new FileWriter(file);
        car.toJson().writeJSONString(writer);
        writer.close();
    }

    public List<Car> index() {
        var directory = new File(this.directory);
        return Arrays.stream(Objects.requireNonNull(directory.listFiles())).filter(file -> file.getName().endsWith(".json")).map(file -> {
            try {
                return this.retrieve(file.getName().replaceAll(".json", ""));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }).toList();
    }

    public Car retrieve(String id) throws Exception {
        var file = getCarPath(id).toFile();
        if (!file.exists()) {
            throw new Exception("This car does not exist!");
        }

        var reader = new BufferedReader(new FileReader(file));

        var parser = new JSONParser();
        var car =  Car.fromJson((JSONObject)parser.parse(reader));
        reader.close();
        return car;
    }

    public void delete(Car car) throws IOException {
        Files.delete(getCarPath(car));
    }

    private Path getCarPath(Car car) {
        return this.getCarPath(car.getId());
    }

    private Path getCarPath(UUID id) {
        return this.getCarPath(id.toString());
    }

    private Path getCarPath(String id) {
        return Paths.get(directory, id + ".json");
    }
}
