package marin.calin.lab7.ex4.app.models;

import org.json.simple.JSONObject;

import java.io.Serializable;
import java.util.UUID;

public class Car implements Serializable {
    private final UUID id;
    private String model;
    private double price;

    public Car(String model, double price) {
        this.id = UUID.randomUUID();
        this.model = model;
        this.price = price;
    }

    public Car(String model, double price, String id) {
        this.id = UUID.fromString(id);
        this.model = model;
        this.price = price;
    }

    public Car(String model, double price, UUID id) {
        this.model = model;
        this.price = price;
        this.id = id;
    }

    public static Car fromJson(JSONObject object) {
        return new Car(object.get("model").toString(), Double.parseDouble(object.get("price").toString()), object.get("id").toString());
    }

    public UUID getId() {
        return id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Car { " +
                "id = " + id +
                ", model = '" + model + '\'' +
                ", price = " + price +
                " }";
    }

    public JSONObject toJson() {
        var object = new JSONObject();
        object.put("id", getId().toString());
        object.put("model", getModel());
        object.put("price", getPrice());
        return object;
    }
}
