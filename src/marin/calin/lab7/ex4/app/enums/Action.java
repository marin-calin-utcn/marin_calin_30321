package marin.calin.lab7.ex4.app.enums;

public enum Action {
    Index,
    Create,
    Read,
    Delete,
    Quit
}
