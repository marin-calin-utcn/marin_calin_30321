package marin.calin.lab7.ex4.app;

import marin.calin.lab7.ex4.app.enums.Action;
import marin.calin.lab7.ex4.app.models.Car;
import marin.calin.lab7.ex4.app.services.MenuService;
import marin.calin.lab7.ex4.app.services.StorageService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {
    public static String Directory = "data/lab7/ex4/cars";

    private final MenuService menuService;
    private final StorageService storageService;

    public App() {
        this.menuService = new MenuService();
        this.storageService = new StorageService(Directory);
    }

    public void run() throws IOException {
        Action action;
        do {
            menuService.showMenu();
            action = menuService.getAction();

            switch (action) {
                case Index -> {
                    storageService.index().forEach(System.out::println);
                }
                case Create -> {
                    var reader = new BufferedReader(new InputStreamReader(System.in));
                    System.out.println("What model do you want the car to have?");
                    var model = reader.readLine();
                    System.out.println("How much do you want the car to cost?");
                    var price = Double.parseDouble(reader.readLine());
                    storageService.store(new Car(model, price));
                }
                case Read -> {
                    var reader = new BufferedReader(new InputStreamReader(System.in));
                    System.out.println("Search for a car model:");
                    var query = reader.readLine();
                    var results = storageService.index().stream().filter(car -> car.getModel().toLowerCase().contains(query.toLowerCase())).toList();
                    System.out.println("Results found: " + results.size());
                    results.forEach(System.out::println);
                }
                case Delete -> {
                    var reader = new BufferedReader(new InputStreamReader(System.in));
                    System.out.println("Delete by car model or id:");
                    var query = reader.readLine();
                    var results = storageService.index().stream()
                            .filter(car -> car.getModel().toLowerCase().contains(query.toLowerCase()) || car.getId().toString().equals(query))
                            .toList();
                    if (results.size() > 1) {
                        System.out.println("More than one result was found. Please refine your search!");
                    } else if (results.size() < 1) {
                        System.out.println("No results were found!");
                    } else {
                        var car = results.stream().findFirst().orElseThrow();
                        try {
                            storageService.delete(car);
                            System.out.println(car + " removed.");
                        } catch (IOException e) {
                            System.out.println(car + " could not be removed.");
                            System.out.println(e.getMessage());
                        }
                    }
                }
            }
        } while (action != Action.Quit);
    }
}
