package marin.calin.lab7.ex2;

public class App {
    public final char character;

    public App(char character) {
        this.character = character;
    }

    public long countAppearances(String text) {
        return text.chars().filter(c -> c == this.character).count();
    }
}
