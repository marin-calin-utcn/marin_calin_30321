package marin.calin.lab7.ex2;

import java.io.*;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) throws IOException {

        // Read the character
        char character = getCharacter();

        // Get the input text
        var data = getInput();

        // Count the number of appearances
        var app = new App(character);
        var appearances = app.countAppearances(data);

        // Print the result
        System.out.println("The character '" + character + "' appears " + appearances + " times in the input text.");
    }

    private static String getInput() throws FileNotFoundException {
        var file = new File("E:\\Marin_Calin_30321\\src\\marin\\calin\\lab7\\ex2\\data.txt");
        var reader = new Scanner(file);
        var data = "";
        while (reader.hasNextLine()) {
            data = data.concat(reader.nextLine());
        }
        reader.close();
        return data;
    }

    private static char getCharacter() throws IOException {
        var reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("What character do you want to look for?");
        var character = (char) reader.read();
        reader.close();
        return character;
    }
}
